package my.atherox.asteroidsnaturamil;

import com.badlogic.gdx.graphics.g2d.Sprite;

public interface Renderable {
    public void render();
}
