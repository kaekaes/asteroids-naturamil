package my.atherox.asteroidsnaturamil.GameObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;

import java.util.Random;

import my.atherox.asteroidsnaturamil.AsteroidsNaturaMil;
import my.atherox.asteroidsnaturamil.Renderable;
import my.atherox.asteroidsnaturamil.Updateable;

public class Meteor extends GameObject implements Renderable, Updateable {

    private Polygon polygon;
    private float[] verts;
    private int actVert;
    private float orbit;
    private static Sound flash;
    private boolean hitable =true;

    private Sprite sprite;

    int textureId = 1;
    float textureChange = 0.2f, actualTexturechange = 0;
    int actualSprite = 1;

    public Meteor(float x, float y) {
        super(x, y);
        orbit = MathUtils.random(0, 180);
        speed = 0.3f * AsteroidsNaturaMil.get().scale +(new Random().nextFloat()) * AsteroidsNaturaMil.get().scale;
        textureId = new Random().nextInt(2) + 1;
        sprite = AsteroidsNaturaMil.ta.createSprite("Pajaro" + textureId + actualSprite);
        sprite.scale(1.5f);
        createVerts();
        polygon = new Polygon(verts);
        polygon.setPosition(x, y);
        polygon.setRotation((MathUtils.atan2((float) Math.sin(orbit) + speed, (float) Math.cos(orbit) + speed)) * MathUtils.radiansToDegrees - 90);


//        if(flash==null)
//            flash = Gdx.audio.newSound(Gdx.files.internal("camera.mp3")); //TODO sonido
//        AsteroidsNaturaMil.get().getMeteors().add(this);
    }

    //DE UNA LIBRERÍA DE GRÁFICOS
    private void createVerts() {
        float sWidth = sprite.getWidth() * 2;
        float sHeight = sprite.getHeight() * 2;
        float a = 2 / 1.2f;
        verts = new float[]{
                -sWidth / a, -sHeight / a,
                sWidth / a, -sHeight / a,
                sWidth / a, sHeight / a,
                -sWidth / a, sHeight / a,
                -sWidth / a, -sHeight / a
        };
    }

    public void wrapBorder() {
        float x = polygon.getX();
        float y = polygon.getY();

        float width = AsteroidsNaturaMil.get().getWidth();
        float height = AsteroidsNaturaMil.get().getHeight();
        float sWidth = sprite.getWidth();
        float sHeight = sprite.getHeight();

        if (x > width + sWidth / 2)
            x = 0 - sWidth / 2;
        if (x < 0 - sWidth / 2)
            x = width + sWidth / 2;
        if (y > height + sHeight / 2)
            y = 0 - sHeight / 2;
        if (y < 0 - sHeight / 2)
            y = height + sHeight / 2;
        polygon.setPosition(x, y);
    }

    public void move() {
        x = (float) Math.cos(orbit) + speed;
        y = (float) Math.sin(orbit) + speed;

        actualTexturechange += Gdx.graphics.getDeltaTime();
        if (actualTexturechange >= textureChange) {
            actualSprite = actualSprite == 1 ? 2 : 1;
            sprite = AsteroidsNaturaMil.ta.createSprite("Pajaro" + textureId + actualSprite);
            sprite.setRotation(polygon.getRotation());
            sprite.scale(1.5f);
            sprite.scale(sprite.getScaleX()+sprite.getScaleY()/2*AsteroidsNaturaMil.get().scale);
            actualTexturechange = 0;
        }

    }

    public boolean contains(float x, float y) {
        return polygon.contains(x, y);
    }

    @Override
    public void render() {
        sprite.setPosition(polygon.getX(), polygon.getY());
        sprite.setRotation(polygon.getRotation());
        sprite.draw(AsteroidsNaturaMil.batch);

//        ShapeRenderer sr = new ShapeRenderer();
//        sr.begin(ShapeRenderer.ShapeType.Line);
//        sr.polygon(polygon.getTransformedVertices());
//        sr.end();
    }

    @Override
    public void update() {
        move();
        polygon.translate(x, y);
        wrapBorder();
    }

    @Override
    public void delete() {
        super.delete();
    }


    public void hit() {
        AsteroidsNaturaMil.get().getMeteors().remove(this);
        delete();
    }

    public float getRealX(){
        return polygon.getX();
    }

    public float getRealY(){
        return polygon.getY();
    }


    public boolean hitable(){
        return hitable;
    }
    public void unHitable(){
        hitable = false;
    }
}
