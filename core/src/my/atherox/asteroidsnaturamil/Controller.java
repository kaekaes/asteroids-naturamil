package my.atherox.asteroidsnaturamil;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Controller {
    Viewport viewport;
    Stage stage;
    Stage stageMenu;
    boolean upPressed, leftPressed, rightPressed, shootPressed, startGame;
    OrthographicCamera cam;

    public Controller() {
        cam = new OrthographicCamera();
        viewport = new FitViewport(AsteroidsNaturaMil.get().getWidth(), AsteroidsNaturaMil.get().getHeight(), cam);
        stage = new Stage(viewport, AsteroidsNaturaMil.batch);

        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                switch (keycode) {
                    case Input.Keys.W:
                        upPressed = true;
                        break;
                    case Input.Keys.A:
                        leftPressed = true;
                        break;
                    case Input.Keys.D:
                        rightPressed = true;
                        break;
                    case Input.Keys.SPACE:
                        shootPressed = true;
                        break;
                }
                return true;
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                switch (keycode) {
                    case Input.Keys.W:
                        upPressed = false;
                        break;
                    case Input.Keys.A:
                        leftPressed = false;
                        break;
                    case Input.Keys.D:
                        rightPressed = false;
                        break;
                    case Input.Keys.SPACE:
                        shootPressed = false;
                        break;
                }
                return true;
            }


        });

        Table table = new Table();
        table.left().bottom();

        Image upImg = new Image(new Texture("flatDark25.png"));
        upImg.setSize(50*AsteroidsNaturaMil.get().scale, 50*AsteroidsNaturaMil.get().scale);
        upImg.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                upPressed = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                upPressed = false;
            }
        });

        Image rightImg = new Image(new Texture("flatDark24.png"));
        rightImg.setSize(50*AsteroidsNaturaMil.get().scale, 50*AsteroidsNaturaMil.get().scale);
        rightImg.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                rightPressed = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                rightPressed = false;
            }
        });

        Image leftImg = new Image(new Texture("flatDark23.png"));
        leftImg.setSize(50*AsteroidsNaturaMil.get().scale, 50*AsteroidsNaturaMil.get().scale);
        leftImg.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                leftPressed = true;
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                leftPressed = false;
            }
        });

        Image shootImg = new Image(new Texture("flatDark29.png"));
        shootImg.setSize(50*AsteroidsNaturaMil.get().scale, 50*AsteroidsNaturaMil.get().scale);
        shootImg.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println(shootPressed);
                if (shootPressed)
                    return false;
                else {
                    shootPressed = true;
                    return true;
                }
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                System.out.println("LIFT");
                shootPressed = false;
            }
        });

        table.add();
        table.add(upImg).size(upImg.getWidth(), upImg.getHeight());
        table.add();
        table.row().pad(5*AsteroidsNaturaMil.get().scale);
        table.add(leftImg).size(leftImg.getWidth(), leftImg.getHeight());
        table.add(shootImg).size(shootImg.getWidth(), shootImg.getHeight());
        table.add(rightImg).size(rightImg.getWidth(), rightImg.getHeight());
        for (int i = 0; i < 50; i++) {
            table.add();
        }
        table.add(shootImg).size(shootImg.getWidth(), shootImg.getHeight());
        table.row().padBottom(5*AsteroidsNaturaMil.get().scale);
        table.add();
        table.add();
        table.add();

        stage.addActor(table);

        stageMenu = new Stage(viewport, AsteroidsNaturaMil.batch);

        stageMenu.addListener(new InputListener(){
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if(keycode == Input.Keys.SPACE || keycode == Input.Keys.ENTER){
                    startGame = true;
                    System.out.println("aaaaaaa");
                    return true;
                }
                return false;
            }
        });

        final Image titleImg = new Image(new Texture("title2.png"));
        titleImg.setSize(300*AsteroidsNaturaMil.get().scale, 100*AsteroidsNaturaMil.get().scale);

        final Image startImg = new Image(new Texture("MainMenuPlay.png"));
        startImg.setSize(150*AsteroidsNaturaMil.get().scale, 50*AsteroidsNaturaMil.get().scale);
        startImg.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                startGame = true;
                System.out.println("aaaaaaaaaaaaa");
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                startGame = false;
            }
        });

        Table tableMM = new Table();
        tableMM.setPosition(AsteroidsNaturaMil.get().getWidth()/2, 0);
        tableMM.row().padBottom(AsteroidsNaturaMil.get().getHeight()/5);
        tableMM.add(titleImg).size(titleImg.getWidth(), titleImg.getHeight());
        tableMM.row().padBottom(AsteroidsNaturaMil.get().getHeight());
        tableMM.add(startImg).size(startImg.getWidth(), startImg.getHeight());

        stageMenu.addActor(tableMM);

    }

    public void draw() {
        stage.draw();
    }

    public void drawMenu(){
        stageMenu.draw();
    }

    public boolean isUpPressed() {
        return upPressed;
    }

    public boolean isLeftPressed() {
        return leftPressed;
    }

    public boolean isRightPressed() {
        return rightPressed;
    }

    public boolean hasGameStart(){
        return startGame;
    }

    public boolean isShootPressed() {
        if (shootPressed) {
            shootPressed = false;
            return true;
        }
        return false;
    }

    public void changeSceneInput(boolean mainMenu){
        if(mainMenu)
            Gdx.input.setInputProcessor(stageMenu);
        else
            Gdx.input.setInputProcessor(stage);

    }

    public void resize(int width, int height) {
        viewport.update(width, height);
    }
}